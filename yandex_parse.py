from urllib2 import Request
from bs4 import BeautifulSoup
import requests

def get_temp(s):
    mode = 0
    t1 = ''
    t2 = ''
    for x in s:
        if x in '+-':
            mode += 1
            if mode == 1:
                t1 += x
            elif mode == 2:
                t2 += x
        if x in '0123456789':
            if mode == 1:
                t1 += x
            elif mode == 2:
                t2 += x
    return (float(t1) + float(t2)) / 2.0


def get_html(url):
    resp = requests.get(url)
    return resp.content


def parse(html, day):
    soup = BeautifulSoup(html, 'lxml')
    content = soup.find('div', class_='tabs-panes__pane tabs-panes__pane_active_yes').dl
    html2 = str(content)
    soup = BeautifulSoup(html2, 'lxml')
    dt_list = soup.find_all('dt')
    dd_list = soup.find_all('dd')

    forecast_index = 0
    for i in xrange(len(dt_list)):
        if dt_list[i]['data-anchor'] == str(day):
            forecast_index = i
            break

    data = dd_list[forecast_index].table.tbody
    pres = data.find_all('td', class_='weather-table__body-cell weather-table__body-cell_type_air-pressure')[1].div.text
    temp = data.find_all('td', class_='weather-table__body-cell weather-table__body-cell_type_daypart')[1].text
    temp = get_temp(temp.encode('utf-8'))
    humdt = data.find_all('td', class_='weather-table__body-cell weather-table__body-cell_type_humidity')[1].div.text
    humdt = float(humdt[:-1])
    return (temp, float(pres), humdt)





url = 'https://pogoda.yandex.ru/London/details?from=serp_title' #testing
day = 4

print(parse(get_html(url), day))