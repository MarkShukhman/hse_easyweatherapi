from sqlalchemy import create_engine, Table, Column, Integer, Float, String, MetaData, ForeignKey
from sqlalchemy.orm import mapper
from tables import *

engine = create_engine('sqlite:///database.db', echo = True)
metadata = MetaData()

services_table = Table('services', metadata,
                       Column('service_id', Integer, primary_key=True),
                       Column('service_name', String(50))
                       )

sources_table = Table('sources', metadata,
                      Column('geo_id', Integer, primary_key=True),
                      Column('services_id', Integer),
                      Column('url', String(50))
                      )

geo_table = Table('geo', metadata,
                  Column('geo_id', Integer, primary_key=True),
                  Column('name', String(50))
                  )

data_table = Table('data', metadata,
                   Column('geo_id', Integer, primary_key=True),
                   Column('service_id', Integer),
                   Column('measure_date', String(50)),
                   Column('forecast_date', String(50)),
                   Column('temp', Float),
                   Column('pres', Float),
                   Column('humdt', Float)
                   )

forecast_table = Table('forecast', metadata,
                       Column('geo_id', Integer, primary_key=True),
                       Column('forecast_date', String(50)),
                       Column('temp', Float),
                       Column('pres', Float),
                       Column('humdt', Float)
                       )


mapper(Service, services_table)
mapper(Source, sources_table)
mapper(Geo, geo_table)
mapper(Data, data_table)
mapper(Forecast, forecast_table)

metadata.create_all(engine)
