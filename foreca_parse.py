from bs4 import BeautifulSoup
from urllib import request

def get_html(url):
    resp = request.urlopen(url)
    return resp.read()


def parse(html):
    soup = BeautifulSoup(html, "html.parser")
    content = soup.find('div', class_='left')

    status = int(content.span.strong.text)
    return (status, )
    """
    content = soup.find('div', class_='right txt-tight')
    content = content.find_all('strong')
    status = 'Ощущается как: ' + content[0].text + 'С'
    print(status)
    status = 'Относительная влажность: ' + content[3].text + '%'
    print(status)
    status = 'Восход солнца: ' + content[4].text
    print(status)
    status = 'Закат солнца: ' + content[5].text
    print(status)
    status = 'Долгота дня: ' + content[6].text
    print(status)"""




def main():
    base = 'http://foreca.ru/'
    while (True):
        country = input('Введите страну: ') + '/'
        if (country == 'DONE/'):
            break
        city = input('Введите город: ')
        url = base + country + city
        print(parse(get_html(url)))


if __name__ == '__main__':
    main()
